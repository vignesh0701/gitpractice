
package Practice;

public class SwitchStatement {
    public static void main(String[] args) {
        //switch statement passing throgh the different datatype value like int double 
        //enum boolean also
        //inside the switch statement create multiple case lables each case file 
        //having diff statement inside the block of code
        //if i need some or particular statement in output that time using break 
        //keyword its optional
        
        char c ='D';
        
        switch(c){
            case 'A':
                System.out.println("Apple");
                break;
            case 'B':
                System.out.println("Ball");
                break;
            case 'C':
                System.out.println("Catch");
                break;
            default :
                System.out.println("Not");
            case 'D':
                System.out.println("Dan");
        }
            
    }
}
