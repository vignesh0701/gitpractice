public class DataTypeDemo {
    
    
    public static void main(String[] args) {
        byte b = 125;
        short s = 255;
        int i = 4567890;
        long l = 3245671893L;
        float f = 456.781F;
        double d = 3456789.45667;
        boolean bool = true;
        String str = "Hello";
        
        System.out.println(b);
        System.out.println(b + 2);
        System.out.println(l);
        System.out.println(s * 2);
        System.out.println(str);
    }
}

