
package Practice;

public class UnaryOperator {
    public static void main(String[] args) {
        //post ++ -- operation curent value is used then increment,decrement is performed
        //++ -- pre operation is first increment,decrement is done then used in operation
        
        int i =20;
        int j=12;
        
        System.out.println((i++)+ (--j)*(--i));
        i++;
        System.out.println((i)+(j++));
        j--;
        System.out.println((j)-(--j)+(i--));
        
        int k = 8;
        k--;
        
        System.out.println(true);
        
        System.out.println(false);
        
       
    }
  
}
