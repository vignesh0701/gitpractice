package basics;
public class DataTypeDemo {
    \u0070ublic static void main(String[] args) {
        var b1 = 125;
          b1 = 200;
         char h = 65;
        short s = 255;
        char c='\u0061';
        
        int i = 4567890;
        long l = 3245671893L;
        float f = 456.781F;
        double d = 3456789.45667;
        boolean bool = true;
        String str = "Hello";
        
        System.out.println(h);
        System.out.println(c);
        System.out.println(b1);
        System.out.println(b1 + 2);
        System.out.println(l);
        System.out.println(s * 2);
        System.out.println(str);
    }
}
